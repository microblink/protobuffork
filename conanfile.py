from conan import ConanFile
from conan.tools.files import rename, rmdir, copy

import os


class ProtobufConan(ConanFile):
    name = "protobuf"
    license = "BSD"
    description = "Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data"
    url = "https://github.com/microblink/protobuf"

    python_requires = 'conanfile_utils/[~1]@microblink/main'
    python_requires_extend = 'conanfile_utils.MicroblinkConanFile'

    def requirements(self):
        self.requires('abseil/[>=20230802]@microblink/main', transitive_headers=True)

    @property
    def _cmake_install_base_path(self):
        if self.settings.os == 'Windows':
            return 'cmake'
        else:
            return os.path.join("lib", "cmake", "protobuf")

    @property
    def _has_protoc(self):
        return self.settings.os in ['Linux', 'Macos', 'Windows']

    def build(self):
        # invoked only when building package
        build_only_cmake_vars = {
            'protobuf_BUILD_PROTOC_BINARIES': self._has_protoc
        }
        self.mb_build_target(build_only_vars=build_only_cmake_vars)

    def package(self):
        super().package()

        # remove files that are not used and just confuse the user
        rmdir(self, os.path.join(self.package_folder, "lib", "pkgconfig"))
        rmdir(self, os.path.join(self.package_folder, "lib", "cmake", 'utf8_range'))
        os.unlink(os.path.join(self.package_folder, self._cmake_install_base_path, "protobuf-config-version.cmake"))
        os.unlink(os.path.join(self.package_folder, self._cmake_install_base_path, "protobuf-targets.cmake"))
        os.unlink(os.path.join(self.package_folder, self._cmake_install_base_path, "protobuf-targets-{}.cmake".format(str(self.settings.build_type).lower())))

        # avoid conflict with files that conan generates
        rename(
            self,
            os.path.join(self.package_folder, self._cmake_install_base_path, "protobuf-config.cmake"),
            os.path.join(self.package_folder, self._cmake_install_base_path, "protobuf-utils.cmake")
        )

        if self.settings.os == 'Macos':
            cmake_generator = self.conf.get('tools.cmake.cmaketoolchain:generator', default='Xcode')
            if cmake_generator == 'Xcode':
                # workaround for https://gitlab.kitware.com/cmake/cmake/-/issues/21854
                # use the original protoc binary instead of the installed one, as install_name_tool
                # invalidates the signature and this makes binary unstartable
                copy(
                    self,
                    pattern='protoc',
                    src=os.path.join(self.build_folder, 'protobuf', str(self.settings.build_type)),
                    dst=os.path.join(self.package_folder, 'bin')
                )


    def package_info(self):
        # compatibility with conan-center package
        self.cpp_info.set_property("cmake_find_mode", "both")
        self.cpp_info.set_property("cmake_module_file_name", "Protobuf")
        self.cpp_info.set_property("cmake_file_name", "protobuf")

        build_modules = [
            os.path.join(self._cmake_install_base_path, "protobuf-utils.cmake"),
            os.path.join(self._cmake_install_base_path, "protobuf-module.cmake"),
            os.path.join(self._cmake_install_base_path, "protobuf-options.cmake"),
        ]
        self.cpp_info.set_property("cmake_build_modules", build_modules)

        lib_prefix = "lib" if self.settings.os == 'Windows' else ""

        # taken from protobuf/cmake/abseil-cpp.cmake
        absl_used_targets = [
            'abseil::absl_absl_check',
            'abseil::absl_absl_log',
            'abseil::absl_algorithm',
            'abseil::absl_base',
            'abseil::absl_bind_front',
            'abseil::absl_bits',
            'abseil::absl_btree',
            'abseil::absl_cleanup',
            'abseil::absl_cord',
            'abseil::absl_core_headers',
            'abseil::absl_debugging',
            'abseil::absl_die_if_null',
            'abseil::absl_dynamic_annotations',
            'abseil::absl_flags',
            'abseil::absl_flat_hash_map',
            'abseil::absl_flat_hash_set',
            'abseil::absl_function_ref',
            'abseil::absl_hash',
            'abseil::absl_layout',
            'abseil::absl_log_initialize',
            'abseil::absl_log_severity',
            'abseil::absl_memory',
            'abseil::absl_node_hash_map',
            'abseil::absl_node_hash_set',
            'abseil::absl_optional',
            'abseil::absl_span',
            'abseil::absl_status',
            'abseil::absl_statusor',
            'abseil::absl_strings',
            'abseil::absl_synchronization',
            'abseil::absl_time',
            'abseil::absl_type_traits',
            'abseil::absl_utility',
            'abseil::absl_variant',
        ]

        # libutf8_range
        self.cpp_info.components["libutf8_range"].set_property("cmake_target_name", "utf8_range::utf8_range")
        self.cpp_info.components["libutf8_range"].set_property("pkg_config_name", "utf8_range")
        self.cpp_info.components["libutf8_range"].libs = ['utf8_range']

        # libutf8_range_validity
        self.cpp_info.components["libutf8_range_validity"].set_property("cmake_target_name", "utf8_range::utf8_validity")
        self.cpp_info.components["libutf8_range_validity"].set_property("pkg_config_name", "utf8_range_validity")
        self.cpp_info.components["libutf8_range_validity"].libs = ['utf8_validity']
        self.cpp_info.components["libutf8_range_validity"].requires = ['abseil::absl_strings', 'libutf8_range']

        # libprotobuf
        self.cpp_info.components["libprotobuf"].set_property("cmake_target_name", "protobuf::libprotobuf")
        self.cpp_info.components["libprotobuf"].set_property("pkg_config_name", "protobuf")
        self.cpp_info.components["libprotobuf"].builddirs.append(self._cmake_install_base_path)
        self.cpp_info.components["libprotobuf"].libs = [f'{lib_prefix}protobuf']
        self.cpp_info.components["libprotobuf"].defines = ['GOOGLE_PROTOBUF_NO_RTTI']
        self.cpp_info.components["libprotobuf"].requires = absl_used_targets + ['libutf8_range_validity']
        if self.settings.os == 'Linux':
            self.cpp_info.components["libprotobuf"].system_libs.extend(["m", "pthread"])
        if self.settings.os == "Android":
            self.cpp_info.components["libprotobuf"].system_libs.append("log")

        # libprotobuf-lite
        self.cpp_info.components["libprotobuf-lite"].set_property("cmake_target_name", "protobuf::libprotobuf-lite")
        self.cpp_info.components["libprotobuf-lite"].set_property("pkg_config_name", "protobuf-lite")
        self.cpp_info.components["libprotobuf-lite"].builddirs.append(self._cmake_install_base_path)
        self.cpp_info.components["libprotobuf-lite"].libs = [f'{lib_prefix}protobuf-lite']
        self.cpp_info.components["libprotobuf-lite"].defines = ['GOOGLE_PROTOBUF_NO_RTTI']
        self.cpp_info.components["libprotobuf-lite"].requires = absl_used_targets + ['libutf8_range_validity']
        if self.settings.os == 'Linux':
            self.cpp_info.components["libprotobuf"].system_libs.extend(["m", "pthread"])
        if self.settings.os == "Android":
            self.cpp_info.components["libprotobuf"].system_libs.append("log")

        if self._has_protoc:
            # libprotoc
            self.cpp_info.components["libprotoc"].set_property("cmake_target_name", "protobuf::libprotoc")
            self.cpp_info.components["libprotoc"].libs = [f'{lib_prefix}protoc']
            self.cpp_info.components["libprotoc"].requires = ["libprotobuf"]

# pylint: skip-file
