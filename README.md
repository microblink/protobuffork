Protocol Buffers - Google's data interchange format
===================================================

Please read [protobuf README](protobuf/README.md) for details. Here are only conan-related release notes.

# Release notes

## 4.25.2 

- updated to upstream protobuf v4.25.2 
- allow building protoc/libprotoc also for non-release builds
    - this is a requirement for grpc

## 4.25.1 

- updated to latest upstream protobuf version
- protobuf now depends on the abseil library

## 3.21.7

- packaged into conan v2 format
- recipe is based on conan-center
- the package now contains both protoc, libprotobuf and libprotobuf-lite
- restore using conan-center-compatible version scheme

# Conan V1 names: Protobuf and Protoc

## 21.7

- fixed incorrect packaging of `CMakeLists.txt` and referencing it from `protobuf-src.cmake`
- updated all dependencies
- updated to upstream v21.7

## 21.6

- updated to upstream v21.6
- setup fork as submodule
