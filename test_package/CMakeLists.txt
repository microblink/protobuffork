cmake_minimum_required( VERSION 3.25 )

project( ProtobufTest )

find_package( cmake_build REQUIRED )

build_mobile_exe( ProtobufTest ${CMAKE_CURRENT_LIST_DIR} main.cpp addressbook.proto )

target_include_directories( ProtobufTest PRIVATE "${CMAKE_CURRENT_BINARY_DIR}" )

find_package( Protobuf REQUIRED )

mb_import_protoc()

protobuf_generate_cpp( PROTO_SRCS PROTO_HDRS TARGET ${PROJECT_NAME} )
protobuf_generate( LANGUAGE cpp TARGET ProtobufTest PROTOS addressbook.proto )

target_link_libraries( ProtobufTest PRIVATE protobuf::libprotobuf )
