from conan import ConanFile

class TestProtobufConan(ConanFile):
    python_requires = 'conanfile_utils/[~1]@microblink/main'
    python_requires_extend = 'conanfile_utils.MicroblinkConanFile'

    def requirements(self):
        self.requires(self.tested_reference_str)

    def build_requirements(self):
        self.tool_requires(self.tested_reference_str)

    def test(self):
        self.mb_run_test('ProtobufTest')
