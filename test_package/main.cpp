/**
 * Copyright (c) Microblink Ltd. All rights reserved.
 *
 * ANY UNAUTHORIZED USE OR SALE, DUPLICATION, OR DISTRIBUTION
 * OF THIS PROGRAM OR ANY OF ITS PARTS, IN SOURCE OR BINARY FORMS,
 * WITH OR WITHOUT MODIFICATION, WITH THE PURPOSE OF ACQUIRING
 * UNLAWFUL MATERIAL OR ANY OTHER BENEFIT IS PROHIBITED!
 * THIS PROGRAM IS PROTECTED BY COPYRIGHT LAWS AND YOU MAY NOT
 * REVERSE ENGINEER, DECOMPILE, OR DISASSEMBLE IT.
 */

#include "addressbook.pb.h"

#include <cstdlib>
#include <iostream>

int main()
{
	std::cout << "Mirko Blink\n";

	tutorial::Person p;
	p.set_id( 21 );
	p.set_name( "jenkins" );
	p.set_email( "jenkins@microblink.com" );

	std::cout << p.SerializeAsString() << "\n";
	return EXIT_SUCCESS;
}
