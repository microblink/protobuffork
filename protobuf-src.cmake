include_guard( GLOBAL )

# First import Protobuf library from Tensorflow source - this version is required to support proto3 format
set( PROTOBUF_DIR "${CMAKE_CURRENT_LIST_DIR}/protobuf-src" )

set( PROTOBUF_BUILD_DIR ${CMAKE_BINARY_DIR}/protobuf )

set( protobuf_BUILD_TESTS OFF CACHE BOOL "" )
set( protobuf_BUILD_SHARED_LIBS_DEFAULT OFF CACHE BOOL "" )
set( protobuf_BUILD_SHARED_LIBS OFF CACHE BOOL "" )

set( protobuf_ALLOW_RTTI ${MB_ALLOW_RTTI} CACHE BOOL "" )

set( protobuf_BUILD_PROTOC_BINARIES OFF CACHE BOOL "" )

set( protobuf_WITH_ZLIB OFF CACHE BOOL "" )

add_subdirectory( "${PROTOBUF_DIR}" ${PROTOBUF_BUILD_DIR} )
